<?php

namespace HtmlProcessor\Interfaces;

interface HtmlProcessorInterface {
    public function parse();
    public function render();
    public function stripContent();
    public function stripAttributes();
}
