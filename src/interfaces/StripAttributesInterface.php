<?php

namespace HtmlProcessor\Interfaces;

interface StripAttributesInterface
{
    public function strip($str);
}
