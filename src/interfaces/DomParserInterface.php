<?php

namespace HtmlProcessor\Interfaces;

interface DomParserInterface
{
    public function getElementById($id);
}
