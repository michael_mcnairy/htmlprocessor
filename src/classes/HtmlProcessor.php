<?php

namespace HtmlProcessor\Classes;

/**
  *  HtmlProcessor is a class that parses HTML and retuns specified portions
  *     
  *  HtmlProcessor takes a URL, parses its page's content based on a list of divs
  *  and strips a given list of HTML attributes and returns the resultant HTML.
  *
  * @package  HtmlProcessor
  * @author   Michael MCnairy <michaelmcnairy@hotmail.com>
  * @version  $Revision 1.1 $
  * @access   public
 */
class HtmlProcessor implements \HtmlProcessor\Interfaces\HtmlProcessorInterface {
    private $_allowedElements      = null;
    private $_allowedAttributes    = null;
    private $_content              = ''; //Incoming content to be parsed
    private $_divs                 = [];
    private $_notAllowedAttributes = [];
    private $_output               = null; //Outgoing content to be rendered
    private $_parser               = null;
    private $_stripper             = null;
    private $_notAllowedElements   = null;
    private $_url                  = '';

    /**
     * Class  constructor
     * @param array  $allowedAttributes    Array of allow HTML attributes
     * @param array  $divs       Array of divs to extract from page of HTML
     * @param array  $notAllowedAttributes Array of HTML attributes
     * @param array  $notAllowedElements Array of HTML element tags
     * @param Object $parser     HTML DOM parser
     * @param Object $stripper   HTML attribute stripper 
     * @param string $url        URL of page to be parsed]
     */
    public function __construct(
        $allowedAttributes    = [], 
        $divs                 = [], 
        $notAllowedAttributes = [],
        \HtmlProcessor\Interfaces\DomParserInterface $parser, 
        \HtmlProcessor\Interfaces\StripAttributesInterface $stripper,
        $url
        ) 
    {
        $this->_allowedAttributes    = $allowedAttributes;
        $this->_divs                 = $divs;
        $this->_notAllowedAttributes = $notAllowedAttributes;
        $this->_parser               = $parser;
        $this->_stripper             = $stripper;
        $this->_url                  = $url;
    }

    /**
     * Parse HTML for divs passed into class constructor
     * @return object self
     */
    public function parse()
    {
        $this->_parser->load_file($this->_url);

        foreach($this->_divs as $div)
        {
            $this->_output .= $this->_parser->getElementById("{$div}");
        }

        return $this;
    }

    /**
     * Process and output HTML
     * @param  string $output text to be rendered
     * @return string HTML output
     */
    public function render() {
        echo $this->_output;
    }

    /**
     * Remove _notAllowed HTML attributes from HTML
     * @return object self
     */
    public function stripAttributes()
    {
        foreach($this->_notAllowedAttributes as $na)
        {
            if(in_array($na, $this->_allowedAttributes))
            {
                for($i = 0; $i < count($this->_allowedAttributes); $i++)
                {
                    if($this->_allowedAttributes[$i] == $na)
                    {
                        unset($this->_allowedAttributes[$i]);
                    } 
                }
            }
        }

        $this->_stripper->allow = $this->_allowedAttributes;
        $this->_output = $this->_stripper->strip($this->_output);

        return $this;
    }

    /**
     * Parse HTML DOM on for passing in list of divs
     * @param  Array $divs List of divs to strip from incoming content
     * @return string HTML
     */
    public function stripContent()
    {
        foreach($this->_divs as $div)
        {
            $this->_output .= $this->_parser->getElementById($div);
        }
    }

    /**
     * die and dump
     * @param  mixed $debug Anything to output to screen
     * @return string        debug info
     */
    private static function dd($debug) {
        
        if(!empty($debug))
        {
            print_r($debug);
        }

        $debug = debug_backtrace();

        die("<p style='color: red;font-size: large'>" . __FILE__ . ":" . $debug[0]['line']."</p>");
    }
}
