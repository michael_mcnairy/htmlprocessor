<?php

require __DIR__ . '/vendor/autoload.php';

?>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
    </head>
    <body>
        <?php
            $parser               = new \HtmlProcessor\Classes\SimpleHtmlDom();
            $stripper             = new \HtmlProcessor\Classes\StripAttributes();
            $url                  = 'http://www.stealmylogin.com/';
            $divs                 = ['wrapper'];
            $allowedAttributes    = \HtmlProcessor\Classes\HtmlTags::getHtmlAttributes();
            $notAllowedAttributes = ['style'];
            $import               = new \HtmlProcessor\Classes\HtmlProcessor($allowedAttributes, $divs, $notAllowedAttributes, $parser, $stripper, $url);
            echo  $import->parse()->stripAttributes()->render();
        ?>
    </body>
</html>