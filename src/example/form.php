<?php

require __DIR__ . '/vendor/autoload.php';

?>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
    </head>
    <body>
        <form action="/processor.php" method="POST">
            <label for="url">URL</label>
            <input type="text" name="url">
            <br/><br/>

            <label for="divs" placeholder="div ids separated by commas">Divs</label>
            <input type="text" name="divs"/>
            <br/><br/>

            <label for="divs" placeholder="attributes to remove from elements, default all">Remove Attributes</label>
            <input type="text" name="notAllowedAttributes"/>
            <br/><br/>

            <input type="submit" value="Submit"/>
        </form>
    </body>
</html>