<?php

require __DIR__ . '/vendor/autoload.php';

if(empty($_POST))
{
    echo 'No data given. Return to <a href="http://localhost:8000">form</a>';
    die();
} else {
    $data = $_POST;
}

$parser               = new \HtmlProcessor\Classes\SimpleHtmlDom();
$stripper             = new \HtmlProcessor\Classes\StripAttributes();
$url                  = $data['url'];
$divs                 = explode(',', $data['divs']);
$allowedAttributes    = \HtmlProcessor\Classes\HtmlTags::getHtmlAttributes();
$notAllowedAttributes = explode(',', $data['notAllowedAttributes']);
$import               = new \HtmlProcessor\Classes\HtmlProcessor($allowedAttributes, $divs, $notAllowedAttributes, $parser, $stripper, $url);
echo  $import->parse()->stripAttributes()->render();
